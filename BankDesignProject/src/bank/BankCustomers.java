package bank;

public class BankCustomers {
	
	private static int uniqueCustomerID = 2000000;
	private int idCustomer;
	private String firstName;
	private String lastName;
	
	public BankCustomers(String firstName, String lastName){
		uniqueCustomerID++;
		idCustomer = uniqueCustomerID;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return idCustomer;
	}
}
