package bank;

import java.math.BigDecimal;

public abstract class AllBankAccounts {
	
	private static int uniqueIDNumber = 1000000;
	private int idAccount;
	private int sortCode;
	private int accountNumber;
	private BankCustomers bankCustomers;
	private String accountType;
	private BigDecimal balance;
	private BigDecimal overdraftRate;
	
	public AllBankAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type) throws ExceptionManager{
		// When Sort Code is not 6 digits.
		if (Integer.toString(sortCode).length() != 6){
			throw new ExceptionManager("Invalid sort code. It must be 6 digit numbers. "
					+ "At the moment it has " + Integer.toString(sortCode).length()+ " digits.");
		}
		// When Account Number is not 8 digits.
		if (Integer.toString(accountNumber).length() != 8){
			throw new ExceptionManager("Invalid account number. It must be be 8 digit numbers. "
					+ "At the moment it as " + Integer.toString(accountNumber).length() + " digits");
		}
		
		uniqueIDNumber ++;
		idAccount = uniqueIDNumber;
		this.sortCode = sortCode;
		this.accountNumber = accountNumber;
		this.bankCustomers = bankCustomers;
		this.accountType = type;
	}
	
	public abstract void deposit(BigDecimal amount);
	public abstract void withdraw(BigDecimal amount);
	public abstract void updateBalance();
	
	public int getId() {
		return idAccount;
	}
	
	public int getSortCode() {
		return sortCode;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public BankCustomers getCustomer() {
		return bankCustomers;
	}

	public String getType() {
		return accountType;
	}

	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getOverdraftRate() {
		return overdraftRate;
	}
	public void setOverdraftRate(BigDecimal overdraftRate) {
		this.overdraftRate = overdraftRate;
	}
}
