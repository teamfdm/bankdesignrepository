package bank;

public abstract class PersonalSaverAccounts extends AllBankAccounts {

	public PersonalSaverAccounts(int sortCode, int accountNumber, BankCustomers customer, String type) throws ExceptionManager {
		super(sortCode, accountNumber, customer, type);
	}

	@Override
	public void deposit(BigDecimal amount) {
		if (amount > 0){
			setBalance(getBalance().(amount);
		}
	}

	@Override
	public void withdraw(BigDecimal amount) {
		if (amount <= getBalance()){
			setBalance(getBalance()-amount);
		}
	}
}
