package bank;

import java.util.ArrayList;

public class DataManager {
	
	private ArrayList<AllBankAccounts> accounts = new ArrayList<AllBankAccounts>();
	
	public void createAccount(String type, int sortCode, int accountNumber, BankCustomers bankCustomers){
		if (type.equals("IndividualSavingsAccounts")){
			try {
				accounts.add(new IndividualSavingsAccounts(sortCode,accountNumber,bankCustomers,"IndividualSavingsAccounts"));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
		
		if (type.equals("SavingsAccounts")){
			try {
				accounts.add(new SavingsAccounts(sortCode,accountNumber,bankCustomers,"Savings Account"));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
		
		if (type.equals("CurrentAccounts")){
			try {
				accounts.add(new CurrentAccounts(sortCode,accountNumber,bankCustomers,"Current Account"));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	public void createAccount(String type, int sortCode, int accountNumber, BankCustomers bankCustomers, int overdraftLimit){
		if (type.equals("CurrentAccounts")){
			try {
				accounts.add(new CurrentAccounts(sortCode,accountNumber,bankCustomers,"Current Account",overdraftLimit));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createAccount(String type, int sortCode, int accountNumber, BankCustomers bankCustomers, int overdraftLimit, BigDecimal overdraftRate){
		if (type.equals("CurrentAccounts")){
			try {
				accounts.add(new CurrentAccounts(sortCode,accountNumber,bankCustomers,"Current Account",overdraftLimit,overdraftRate));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createAccount(String type, int sortCode, int accountNumber, BankCustomers bankCustomers, BigDecimal overdraftRate){
		if (type.equals("CurrentAccounts")){
			try {
				accounts.add(new CurrentAccounts(sortCode,accountNumber,bankCustomers,"Current Account",overdraftRate));
			} catch (ExceptionManager e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	

	public ArrayList<AllBankAccounts> getAllAccounts() {
		return accounts;
	}
	
	public AllBankAccounts getAccount(int accountNumber){
		for (AllBankAccounts allBankAccounts : accounts){
			if(allBankAccounts.getAccountNumber() == accountNumber){
				return allBankAccounts;
			}
		}
		
		return null;
	}
	
	public ArrayList<AllBankAccounts> getCustomerAccounts(BankCustomers bankCustomers){
		ArrayList<AllBankAccounts> customerAccounts = new ArrayList<AllBankAccounts>();
		
		for (AllBankAccounts allBankAccounts : accounts){
			if(allBankAccounts.getCustomer().getId() == bankCustomers.getId()){
				customerAccounts.add(allBankAccounts);
			}
		}
		
		return customerAccounts;
	}
	
	

}
