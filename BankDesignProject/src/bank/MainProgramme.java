package bank;

import java.util.ArrayList;

public class MainProgramme {

	public static void main(String[] args) {
		
		// Creating Customers
		BankCustomers rowling = new BankCustomers("J. K.", "Rowling");
		BankCustomers christie = new BankCustomers("Agatha", "Christie");
		BankCustomers cornwell = new BankCustomers("Patricia", "Cornwell");
		BankCustomers james = new BankCustomers("P. D.","James");
		
		DataManager dataManager = new DataManager();
		
		SavingsAccounts.setInterestRate(14.50f);
		IndividualSavingsAccounts.setInterestRate(14.50);
		
		
		// Creating Accounts
		dataManager.createAccount("IndividualSavingsAccounts",            123456, 11111111, rowling);
		dataManager.createAccount("SavingsAccounts", 123456, 22222222, rowling);
		dataManager.createAccount("CurrentAccounts", 654321, 33333333, christie, 1000, 5.0); // overdraft limit set to 1000, interest rate 5%
		dataManager.createAccount("CurrentAccounts", 654321, 44444444, cornwell, 3000, 5.0); // trying to set overdraft limit above max of 2500
		dataManager.createAccount("CurrentAccounts", 654321, 55555555, james, 3000); // overdraft only
		
		
		// Test Case 1 - IndividualSavingsAccounts 
		AllBankAccounts account = dataManager.getAccount(11111111);	
		account.deposit(100.00f);  		// Adding �100
		account.updateBalance(); 	// Calculating Interest
		account.withdraw(50.00f); 	// Withdrawing less than Balance
		account.withdraw(100.00f);	// Trying to withdraw more than Balance
		System.out.println("Test Case 1 - IndividualSavingsAccounts");
		System.out.println("Unique ID           : " + account.getId());
		System.out.println("Account Number      : " + account.getAccountNumber());
		System.out.println("BankCustomers Name       : " + account.getCustomer().getFirstName() + " " + account.getCustomer().getLastName());
		System.out.println("Account Type        : " + account.getType());
		System.out.println("Remaining Balance   : � " + account.getBalance());
		System.out.println();

		// Test Case 2 - Savings Accounts
		account = dataManager.getAccount(22222222);	
		account.deposit(100); // add �100 
		account.updateBalance(); // calculate interest (should now be �120)
		account.withdraw(50.00f); // withdraw less than balance (should now be �70)
		account.withdraw(100.00f); // try to withdraw more than balance (should still be �70)
		System.out.println("Test Case 2 - Savings Accounts");		
		System.out.println("Unique ID           : " + account.getId());
		System.out.println("Account Number      : " + account.getAccountNumber());
		System.out.println("BankCustomers Name       : " + account.getCustomer().getFirstName() + " " + account.getCustomer().getLastName());
		System.out.println("Account Type        : " + account.getType());
		System.out.println("Remaining Balance   : � " + account.getBalance());
		System.out.println();

		// Test Case 3 - Current Accounts (christie)
		account = dataManager.getAccount(33333333);	
		account.withdraw(900); // balance should now be -900
		account.withdraw(200); // balance should still be -900
		System.out.println("Test Case 3 - Current Accounts");			
		System.out.println("Unique ID           : " + account.getId());
		System.out.println("Account Number      : " + account.getAccountNumber());
		System.out.println("BankCustomers Name       : " + account.getCustomer().getFirstName() + " " + account.getCustomer().getLastName());
		System.out.println("Account Type        : " + account.getType());
		System.out.println("Remaining Balance   : � " + account.getBalance());
		account.updateBalance(); // balance should be �945 after interest charged
		System.out.println("After Interest Rate : � " + account.getBalance());
		System.out.println();

		
		// Test current account (cornwell)
		account = dataManager.getAccount(44444444);	
		account.withdraw(2500); // balance should now be -2500
		account.withdraw(200); // balance should still be -2500
		account.setOverdraftRate(3.4f); // changing overdraft rate
		System.out.println("Id: "+account.getId()+", account number: "+account.getAccountNumber()+", customer: "+account.getCustomer().getFirstName()+", type: "+account.getType()+", balance: "+account.getBalance());

		// Test current account (james)
		account = dataManager.getAccount(55555555);	
		account.withdraw(2500); // balance should still be zero as overdraft limit is zero
		System.out.println("Id: " + account.getId()
							+ ", account number: " + account.getAccountNumber()
							+ ", customer: " + account.getCustomer().getFirstName()
							+ ", type: " + account.getType()
							+ ", balance: " + account.getBalance());

		System.out.println("\nDisplaying list of Rowling's accounts: ");
		
		ArrayList<AllBankAccounts> rowlingsAccounts = dataManager.getCustomerAccounts(rowling);
		
		for (AllBankAccounts rowlingAccount : rowlingsAccounts){
			System.out.println("Type: " + rowlingAccount.getType()
								+ ", Account number: " + rowlingAccount.getAccountNumber() 
								+ ", balance: " + rowlingAccount.getBalance());
		}
		
		
	}

}
