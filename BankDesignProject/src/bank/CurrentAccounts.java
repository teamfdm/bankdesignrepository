package bank;

import java.math.BigDecimal;

public class CurrentAccounts extends EverydayAccounts {

	private int overdraftLimit;
	
	public CurrentAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type) throws ExceptionManager{
		super(sortCode, accountNumber, bankCustomers, type);
	}
	
	public CurrentAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type, int overdraftLimit) throws ExceptionManager{
		super(sortCode, accountNumber, bankCustomers, type);
	}
	
	public CurrentAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type, BigDecimal overdraftRate) throws ExceptionManager{
		super(sortCode, accountNumber, bankCustomers, type);
	}
	
	public CurrentAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type, int overdraftLimit, BigDecimal overdraftRate) throws ExceptionManager{
		super(sortCode, accountNumber, bankCustomers, type);
		if (overdraftLimit < 0){
			this.overdraftLimit = 0;
		} else if (overdraftLimit <= 2500){
			this.overdraftLimit = overdraftLimit;
		} else {
			this.overdraftLimit = 2500;
		}
		setOverdraftRate(overdraftRate);
	}

	@Override
	public void updateBalance() {
		BigDecimal interestAmount = getBalance().multiply(getOverdraftRate().divide(new BigDecimal("100")));
		
		if (getBalance().compareTo(BigDecimal.ZERO)  >0){
			setBalance(getBalance().add(interestAmount) );
		}
	}

	@Override
	public void deposit(BigDecimal amount) {
		if (amount > 0){
			setBalance(getBalance()+amount);
		}
	}

	@Override
	public void withdraw(BigDecimal amount) {
		if (amount <= (getBalance()+overdraftLimit)){
			setBalance(getBalance()-amount);
		}
	}

	@Override
	public void deposit(BigDecimal amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void withdraw(BigDecimal amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deposit(BigDecimal amount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void withdraw(BigDecimal amount) {
		// TODO Auto-generated method stub
		
	}

	

	

}
