package bank;

public class IndividualSavingsAccounts extends PersonalSaverAccount {

	private static BigDecimal interestRate;
	
	public IndividualSavingsAccounts(int sortCode, int accountNumber, BankCustomers bankCustomers, String type) throws ExceptionManager{
		super(sortCode, accountNumber, bankCustomers, type);
	}

	public static BigDecimal getInterestRate() {
		return interestRate;
	}

	public static void setInterestRate(BigDecimal interestRate) {
		IndividualSavingsAccounts.interestRate = interestRate;
	}

	@Override
	public void updateBalance() {
		BigDecimal interestRateMultiplier = 1.0 + (interestRate/100);
		setBalance(Math.round((getBalance() * interestRateMultiplier)*100)/100);
	}

}
